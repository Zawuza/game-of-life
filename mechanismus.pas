unit mechanismus;
{Это модуль с механикой игры. Функция NextGen берет двухмерный массив и количество клеток на поле и
возвращает следующее поколение}
{Замечание на весь модуль: поскольку поле квадратное, то шаг индексов между рядами есть корень из длины,
т.е. sqrt(len), т.е. первый ряд начинается с индекса 0,второй с 0+sqrt(len), третий с 0+2*sqrt(len) и.т.д.}

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  TGenMassive = array [0..49,0..49] of boolean;
  TSosedi = array [1..8] of integer;

function NextGen(massive:TGenMassive;len:integer): TGenMassive;
function Sosedi(k,len:integer):TSosedi;

implementation

function NextGen(massive:TGenMassive;len:integer): TGenMassive;
 var i,j,k {счетчики},LifeCount{количество живых клеток}:integer;
     SosedKletki:TSosedi; {Массив соседних клеток}
     newmassive,massive2:array [0..2499] of boolean;
     {Функция преобразует двухмерный массив massive в одномерный massive2
      и создает из него массив следующего поколения newmassive, переделывая
      его обратно в двухмерный
      Это, черт побери, медленно}
begin
   {Переделываем в одномерный массив}
   for i:=0 to Trunc(sqrt(len))-1 do
      for j:=0 to Trunc(sqrt(len))-1 do
           massive2[i*Trunc(sqrt(len))+j]:=massive[j,i];
   {Перебор всех клеток}
  for i:=0 to len-1 do
  begin
       LifeCount:=0;
       SosedKletki:=Sosedi(i,len);  {Поиск соседних клеток, см. функцию ниже}
       for k:=1 to 8 do             {Сколько живых клеток вокруг?}
       begin
            if massive2[SosedKletki[k]]=true then
               Inc(LifeCount);
       end;
       {Условия игры, т.е. ниже принимается решение о том, выживет ли клетка}
       if massive2[i]=true then
          if (LifeCount=2) or (LifeCount=3) then
             newmassive[i]:=true
          else newmassive[i]:=false
       else
          if (LifeCount=3) then
             newmassive[i]:=true
          else newmassive[i]:=false;
  end;
       {Обратно в двухмерный массив}
      for i:=0 to Trunc(sqrt(len))-1 do
         for j:=0 to Trunc(sqrt(len))-1 do begin
             Result[j,i]:=newmassive[i*Trunc(sqrt(len))+j];
end;
end;

function Sosedi(k,len:integer):TSosedi;
{Поиск соседних клеток, исходя из индекса клетки и длины массива}

function rjad(k:integer):integer; begin Result:=k div Trunc(sqrt(len)); end; {Нахождение ряда}

begin
{Существует девять типов клеток по отношению к наличию соседей:
     Клетка 0
     Угловая клетка справа вверху                   \  Массив соседей клетки:
     Угловая клетка слева внизу                     \          123
     Последняя клетка                               \          8r4
     Клетки верхней грани                           \          765
     Клетки левой грани                             \  Пример поля:
     Клетки правой грани                            \          012     len = 9
     Клетки последнего ряда                         \          345     sqrt(len)=3 (расстояние)
     Остальные                                      \          678
 Через оператор if находим все такие                \     (В игре невозможно, минимум 100 клеток}
if k=0 then begin
{Клетка 0}
            Result[1]:=len-1;
            Result[2]:=Trunc(sqrt(len))*(rjad(len)-1);
            Result[3]:=Trunc(sqrt(len))*(rjad(len)-1)+1;
            Result[4]:=1;
            Result[5]:=Trunc(sqrt(len))+1;
            Result[6]:=Trunc(sqrt(len));
            Result[7]:=Trunc(sqrt(len))*2-1;
            Result[8]:=Trunc(sqrt(len))-1;
            exit;
            end;
if k=(Trunc(sqrt(len))-1) then
{Угловая клетка справа вверху}
            begin
            Result[1]:=len-2;
            Result[2]:=len-1;
            Result[3]:=Trunc(sqrt(len))*(rjad(len)-1);
            Result[4]:=0;
            Result[5]:=Trunc(sqrt(len));
            Result[6]:=Trunc(sqrt(len))*2-1;
            Result[7]:=Trunc(sqrt(len))*2-2;
            Result[8]:=Trunc(sqrt(len))-2;
            exit;
            end;
if k=((rjad(len)-1)*Trunc(sqrt(len))) then
{Угловая клетка слева внизу}
            begin
            Result[1]:=k-1;
            Result[2]:=k-Trunc(sqrt(len));
            Result[3]:=k-Trunc(sqrt(len))+1;
            Result[4]:=k+1;
            Result[5]:=1;
            Result[6]:=0;
            Result[7]:=Trunc(sqrt(len))-1;
            Result[8]:=len-1;
            exit;
            end;
if k=len-1 then
{Последняя клетка}
            begin
            Result[1]:=k-Trunc(sqrt(len))-1;
            Result[2]:=k-Trunc(sqrt(len));
            Result[3]:=len-2*Trunc(sqrt(len));
            Result[4]:=(rjad(len)-1)*Trunc(sqrt(len));
            Result[5]:=0;
            Result[6]:=Trunc(sqrt(len))-1;
            Result[7]:=Trunc(sqrt(len))-2;
            Result[8]:=k-1;
            exit;
            end;
if (k>0) and (k<(Trunc(sqrt(len))-1)) then
{Клетки верхней грани (первого ряда)}
            begin
            Result[1]:=(rjad(len)-1)*Trunc(sqrt(len))+k-1;
            Result[2]:=(rjad(len)-1)*Trunc(sqrt(len))+k;
            Result[3]:=(rjad(len)-1)*Trunc(sqrt(len))+k+1;
            Result[4]:=k+1;
            Result[5]:=k+Trunc(sqrt(len))+1;
            Result[6]:=k+Trunc(sqrt(len));
            Result[7]:=k+Trunc(sqrt(len))-1;
            Result[8]:=k-1;
            exit;
            end;
if ((k mod Trunc(sqrt(len)))=0) and (k<>0) and (k<>(rjad(len)-1)*Trunc(sqrt(len))) then
{Клетки левой грани}
            begin
            Result[1]:=k-1;
            Result[2]:=k-Trunc(sqrt(len));
            Result[3]:=k-Trunc(sqrt(len))+1;
            Result[4]:=k+1;
            Result[5]:=k+Trunc(sqrt(len))+1;
            Result[6]:=k+Trunc(sqrt(len));
            Result[7]:=k+2*Trunc(sqrt(len))-1;
            Result[8]:=k+Trunc(sqrt(len))-1;
            exit;
            end;
if ((k mod Trunc(sqrt(len)))=(Trunc(sqrt(len))-1)) and (k<>(Trunc(sqrt(len))-1)) and (k<>(len-1)) then
{Клетки правой грани}
            begin
            Result[1]:=k-Trunc(sqrt(len))-1;
            Result[2]:=k-Trunc(sqrt(len));
            Result[3]:=Trunc(sqrt(len))*(rjad(k)-1);
            Result[4]:=Trunc(sqrt(len))*rjad(k);
            Result[5]:=k+1;
            Result[6]:=k+Trunc(sqrt(len));
            Result[7]:=k+Trunc(sqrt(len))-1;
            Result[8]:=k-1;
            exit;
            end;
if (k>(Trunc(sqrt(len))*(rjad(len)-1))) and (k<(len-1)) then
{Клетки нижней грани (последнего ряда)}
            begin
            Result[1]:=k-Trunc(sqrt(len))-1;
            Result[2]:=k-Trunc(sqrt(len));
            Result[3]:=k-Trunc(sqrt(len))+1;
            Result[4]:=k+1;
            Result[5]:=k-Trunc(sqrt(len))*(rjad(len)-1)+1;
            Result[6]:=k-Trunc(sqrt(len))*(rjad(len)-1);
            Result[7]:=k-Trunc(sqrt(len))*(rjad(len)-1)-1;
            Result[8]:=k-1;
            exit;
            end;
{Остальные}
Result[1]:=k-Trunc(sqrt(len))-1;
Result[2]:=k-Trunc(sqrt(len));
Result[3]:=k-Trunc(sqrt(len))+1;
Result[4]:=k+1;
Result[5]:=k+Trunc(sqrt(len))+1;
Result[6]:=k+Trunc(sqrt(len));
Result[7]:=k+Trunc(sqrt(len))-1;
Result[8]:=k-1;
end;

end.

