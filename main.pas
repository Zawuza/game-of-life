unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Spin, Grids, mechanismus;

type

  { TForm1 }

  TForm1 = class(TForm)
    BGo: TButton;
    DG: TDrawGrid;
    Generations: TLabel;
    Panel: TPanel;
    SE1: TSpinEdit;
    Timer1: TTimer;
    procedure BGoClick(Sender: TObject);
    procedure DGDrawCell(Sender: TObject; aCol, aRow: Integer; aRect: TRect;
      aState: TGridDrawState);
    procedure DGMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SE1Change(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;
  Colormassive:TGenMassive;
  len:integer;
  ongame:boolean;
  stopgame:boolean;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.SE1Change(Sender: TObject);
{Прорисовка квадратиков на основе значения спинэдита}
var i,normalwidth,normalheight:integer;
begin
  normalwidth:=DG.Width div SE1.Value;
  normalheight:=DG.Height div SE1.Value;
  DG.Columns.Clear;
  for i:=1 to SE1.Value do
      DG.Columns.Add;
  DG.RowCount:=SE1.Value;
  DG.DefaultColWidth:=normalwidth;
  DG.DefaultRowHeight:=normalheight;
  len:=sqr(SE1.Value);               {Нахождение длины поля}
end;

procedure TForm1.Timer1Timer(Sender: TObject);
{Основная интерфейсная часть игры:
   1) Отправляет массив в NextGen (модуль mechanismus)
   2) Проверяет, остались ли "живые клетки",
       если да: продолжаем
       если нет: останавливаем}
var i,j,g:integer;
    lives:boolean;
    str:string;
begin
     {Отключение таймера}
     Timer1.Enabled:=false;
     lives:=false;
     {Получение следующего поколения}
     Colormassive:=NextGen(Colormassive,len);
     {Если есть хоть одна живая клетка, то игра продолжается}
     for i:=0 to Trunc(sqrt(len))-1 do
         for j:=0 to Trunc(sqrt(len))-1 do
             if Colormassive[j,i]=true then
                begin
                lives:=true;
                break;
                end;
     {Если игрок остановил игру (см. BGoClick) то игра прекращается}
     if stopgame then
        begin
             stopgame:=false;
             lives:=false;
        end;
     if lives then
        begin
             {Игра продолжается}
             str:=Generations.Caption;
             Delete(str,1,19);
             g:=StrToInt(str);
             Inc(g);
             Generations.Caption:='Поколение: '+ IntToStr(g);
             DG.Repaint;
             Timer1.Enabled:=true;
        end
     else
         begin
              {Игра останавливается}
             str:=Generations.Caption;
             Delete(str,1,19);
             g:=StrToInt(str);
             Showmessage('Gamestop' + #13 + 'Поколений: '+ IntToStr(g));
             ongame:=false;
             SE1.Enabled:=true;
             for i:=0 to 49 do
                 for j:=0 to 49 do
                     Colormassive[i,j]:=false;
             DG.Repaint;
             Generations.Caption:='Поколение: 0';
             BGo.Caption:='Go!'
         end;
end;

procedure TForm1.FormShow(Sender: TObject);
var i,j:integer;
begin
{Инициализация массива, переменных прорисовка клеточек}
    for i:=0 to 49 do
      for j:=0 to 49 do
          Colormassive[i,j]:=false;
  SE1Change(Form1);
  stopgame:=false;
end;

procedure TForm1.DGMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var aCol,aRow:integer;
begin
  {Установка живых и мертвых клеток}
  if ongame then exit;
  aCol:=X div DG.DefaultColWidth;
  aRow:=Y div DG.DefaultRowHeight;
  Colormassive[aCol,aRow]:=not Colormassive[aCol,aRow];
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  {Перерисовка клеточек при
  изменении размера формы}
  SE1Change(Form1);
end;

procedure TForm1.DGDrawCell(Sender: TObject; aCol, aRow: Integer; aRect: TRect;
  aState: TGridDrawState);
begin
  {Выбор цвета клетки}
  if Colormassive[aCol,aRow]=true then
     begin
     DG.Canvas.Brush.Color:=clBlack;
     DG.Canvas.FillRect(DG.CellRect(aCol,aRow));
     end
  else
     begin
     DG.Canvas.Brush.Color:=clWhite;
     DG.Canvas.FillRect(DG.CellRect(aCol,aRow));
     end;
end;

procedure TForm1.BGoClick(Sender: TObject);
begin
  {Основная кнока: включение режима игры,
  отключение ненужных элементов управления}
  if ongame then
     begin
          stopgame:=true;
          exit;
     end;
  SE1.Enabled:=false;
  Timer1.Enabled:=true;
  ongame:=true;
  BGo.Caption:='Stop';
end;

end.

